import pytest

from my_site.blog.models import Post
from my_site.blog.tests.factories import PostFactory


@pytest.fixture
def post() -> Post:
    return PostFactory()
