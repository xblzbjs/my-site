import os

from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Resume(models.Model):
    """Resume model"""

    file = models.FileField(
        _("Resume"),
        upload_to="uploads/resumes/",
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
    )

    @property
    def file_name(self):
        return os.path.basename(self.file.name)

    @property
    def file_size(self) -> str:
        return f"{self.file.size} bytes"

    @property
    def file_url(self):
        return self.file.url

    class Meta:
        db_table = "marketing_resume"
        verbose_name = _("Resume")
        verbose_name_plural = _("Resumes")


class Contact(models.Model):
    """Contact model"""

    from_email = models.EmailField(_("from email"))
    subject = models.CharField(_("subject"), max_length=255)
    name = models.CharField(_("name"), max_length=255)
    message = models.TextField(_("message"))

    class Meta:
        db_table = "marketing_contact"
        verbose_name = _("Contact")
        verbose_name_plural = _("Contacts")

    def __str__(self):
        return f"{self.subject} {self.name} {self.message}"


class Subscription(models.Model):
    """Subscription model"""

    email = models.EmailField(_("email"), unique=True)

    class Meta:
        db_table = "marketing_sub"
        verbose_name = _("Subscription")
        verbose_name_plural = _("Subscriptions")
