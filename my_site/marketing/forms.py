from django import forms
from django.forms import EmailInput, Textarea, TextInput

from .models import Contact, Subscription


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ("name", "subject", "message", "from_email")
        widgets = {
            "name": TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "Name",
                    "aria-required": True,
                    "minlength": "2",
                },
            ),
            "subject": TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "Subject",
                    "aria-required": True,
                }
            ),
            "message": Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "Enter your message",
                    "rows": 10,
                    "cols": 50,
                    "aria-required": True,
                    "style": "height: 300px",
                }
            ),
            "from_email": EmailInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "Email",
                    "aria-required": True,
                }
            ),
        }


class SubscriptionForm(forms.ModelForm):
    class Meta:
        model = Subscription
        fields = ("email",)
        widgets = {
            "email": EmailInput(
                attrs={
                    "class": "form-control me-md-1 semail",
                    "placeholder": "输入邮箱",
                }
            )
        }
