from django.contrib import admin

from .models import Contact, Resume, Subscription


@admin.register(Resume)
class Resume(admin.ModelAdmin):
    list_display = ("file_name", "file_size", "file_url")


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = (
        "subject",
        "name",
        "from_email",
    )
    search_fields = ("message", "subject", "name")


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ("email",)
