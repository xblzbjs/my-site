from django.conf import settings
from django.core.mail import send_mail
from django.http import FileResponse, HttpResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView

from .forms import ContactForm, SubscriptionForm
from .models import Resume


def download_resume(request):
    try:
        resume = Resume.objects.first()
    except Resume.DoesNotExist:
        return HttpResponse("File not found", status=404)

    path_to_file = resume.file.path
    return FileResponse(open(path_to_file, "rb"), as_attachment=True)


class ResumeView(TemplateView):
    template_name = "resume.html"


resume_view = ResumeView.as_view()


class ContactFormView(CreateView):
    template_name = "contact.html"
    form_class = ContactForm
    success_url = "/"

    def form_valid(self, form):
        # It should return an HttpResponse.
        send_mail(
            subject=form.cleaned_data.get("subject").strip(),
            message=form.cleaned_data.get("message").strip(),
            from_email=form.cleaned_data.get("from_email").strip(),
            recipient_list=[settings.EMAIL_ADDRESS],
        )
        return super().form_valid(form)


contact_view = ContactFormView.as_view()


class SubscriptionFormView(CreateView):
    template_name = "blog/post_list.html"
    form_class = SubscriptionForm
    success_url = reverse_lazy("blog:list")

    def form_vaild(self, form):
        send_mail(
            subject="订阅",
            message="订阅测试",
            from_email=form.cleaned_data.get("email").strip(),
            recipient_list=[settings.EMAIL_ADDRESS],
        )
        return super().form_valid(form)
