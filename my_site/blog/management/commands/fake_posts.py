from django.core.management.base import BaseCommand
from django.db import transaction

from my_site.blog.models import Post
from my_site.blog.tests.factories import PostFactory


class Command(BaseCommand):
    help = "Generate blog fake data"

    NUM_POSTS = 100

    @transaction.atomic
    def handle(self, *args, **kwargs):
        self.stdout.write("Deleting old data...")
        models = [Post]
        for m in models:
            m.objects.all().delete()
        self.stdout.write("Deleting successfully")

        self.stdout.write("Creating new posts...")
        for _ in range(self.NUM_POSTS):
            PostFactory()

        self.stdout.write("Create posts successfully...")
