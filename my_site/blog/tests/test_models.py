import pytest

from ..models import Post

pytestmark = pytest.mark.django_db


def test_post_str(post: Post):
    assert str(post) == f"{post.title}"
