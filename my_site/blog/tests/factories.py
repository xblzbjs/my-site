from django.utils import timezone
from factory import Faker
from factory.django import DjangoModelFactory, ImageField
from factory.fuzzy import FuzzyChoice
from mdgen import MarkdownPostProvider

from ..models import Post

Faker.add_provider(MarkdownPostProvider)


STATUS = ["published", "draft"]


class PostFactory(DjangoModelFactory):
    """
    Post factory.
    """

    title = Faker("sentence", nb_words=5, locale="zh_CN")
    slug = Faker("sentence", nb_words=5)
    thumbnail = ImageField(width=1024, height=800, color="green")
    content = Faker("post", size="medium")
    status = FuzzyChoice(STATUS)
    created = Faker(
        "date_time_between", start_date="-30d", tzinfo=timezone.get_current_timezone()
    )
    modified = Faker(
        "date_time_between", start_date="now", tzinfo=timezone.get_current_timezone()
    )

    class Meta:
        model = Post
