from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from markdownx.admin import MarkdownxModelAdmin

from my_site.blog.models import Post


@admin.register(Post)
class PostAdmin(MarkdownxModelAdmin):
    fieldsets = (
        (
            None,
            {
                "fields": ("thumbnail", "title", "slug"),
            },
        ),
        (
            _("Content"),
            {
                "fields": ("content",),
            },
        ),
        (
            _("Other Information"),
            {
                "fields": (
                    ("created", "modified"),
                    ("status", "status_changed"),
                )
            },
        ),
    )

    list_per_page = 20
    list_display = (
        "title",
        "slug",
        "status",
        "status_changed",
        "created",
        "modified",
    )
    list_filter = ("status",)
    search_fields = ("title", "slug")
    readonly_fields = (
        "status_changed",
        "created",
        "modified",
    )

    actions = ["make_published"]

    @admin.display(description=_("Mark selected posts as published"))
    def make_published(self, request, queryset):
        queryset.update(status="published")
