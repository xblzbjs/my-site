from django.urls import path

from .views import blog_view, post_detail_view

app_name = "blog"
urlpatterns = [
    path("", blog_view, name="list"),
    path("subscribe/", blog_view, name="subscribe"),
    path("<slug:slug>/", post_detail_view, name="post-detail"),
]
