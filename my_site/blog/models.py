from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from model_utils import Choices
from model_utils.models import StatusModel, TimeStampedModel
from slugify import slugify


class Post(TimeStampedModel, StatusModel):
    """Post model."""

    STATUS = Choices("draft", "published")

    title = models.CharField(_("title"), max_length=255)
    slug = models.SlugField(_("slug"), max_length=255)
    thumbnail = models.ImageField(
        verbose_name=_("thumbnail"), upload_to="uploads/blog/thumbnails/"
    )
    content = MarkdownxField(_("content"))

    class Meta:
        db_table = "blog_post"
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")

    def __str__(self):
        return f"{self.title}"

    @property
    def formatted_markdown(self):
        return markdownify(self.content)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Get url for post's detail view.
        Returns:
            str: URL for blog post detail
        """
        return reverse("blog:post-detail", kwargs={"slug": self.slug})
