from django.views import View
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView

from my_site.blog.models import Post
from my_site.marketing.forms import SubscriptionForm
from my_site.marketing.views import SubscriptionFormView


class BlogView(View):
    def get(self, request, *args, **kwargs):
        view = PostListView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = SubscriptionFormView.as_view()
        return view(request, *args, **kwargs)


blog_view = BlogView.as_view()


class PostListView(ListView):
    """Post list view."""

    model = Post
    paginate_by = 10
    template_name = "blog/post_list.html"
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super().get_queryset().filter(status="published")
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = SubscriptionForm()
        return context


post_list_view = PostListView.as_view()


class PostDetailView(DetailView):
    """Post detail view."""

    model = Post
    template_name = "blog/post_detail.html"
    context_object_name = "post"


post_detail_view = PostDetailView.as_view()


class AboutView(TemplateView):
    """About view."""

    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["latest_blog_posts"] = Post.objects.filter(status="published").order_by(
            "modified"
        )[:3]
        return context


about_view = AboutView.as_view()
