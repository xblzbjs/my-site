"""my_site URL Configuration"""
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import gettext_lazy as _
from django.views import defaults as default_views

from my_site.blog.views import about_view
from my_site.marketing.views import contact_view, download_resume, resume_view

admin.site.site_header = _("My Site Admin")
admin.site.site_title = _("My Site Admin")
admin.site.index_title = _("Welcome to My Site Portal")

urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("markdownx/", include("markdownx.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns(
    path("admin/", admin.site.urls),
    path("", about_view, name="about"),
    path("contact/", contact_view, name="contact"),
    path("blog/", include("my_site.blog.urls")),
    path("resume/", resume_view, name="resume"),
    path("resume-download/", download_resume, name="resume-download"),
)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
